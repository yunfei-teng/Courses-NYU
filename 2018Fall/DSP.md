Class 9
**The n-order filter has length n+1**
[FIR Filter Design](https://www.mathworks.com/help/signal/ug/fir-filter-design.html)
[Spectrum](https://www.mathworks.com/help/signal/ref/spectrogram.html)
[Parks-McClellan optimal FIR filter order estimation](https://www.mathworks.com/help/signal/ref/firpmord.html)

Class 10
[IIR Filter Design](https://www.mathworks.com/help/signal/ug/iir-filter-design.html)
[butter](https://www.mathworks.com/help/signal/ref/butter.html)
[filter](https://www.mathworks.com/help/matlab/ref/filter.html)
**Question: How can I decide the length of IIR filter? Sum of nominator and denominator coefficients.**

Class 11
Why does Matlab use something totally different for ripples.
[Digital Filter Terminology](https://dspguru.com/dsp/reference/filter-terminology/)
[Ellip Filter](https://www.mathworks.com/help/signal/ref/ellip.html)
[Minimum Order](https://www.mathworks.com/help/signal/ref/ellipord.html)
[freqz](https://www.mathworks.com/help/signal/ref/freqz.html)
[unwrap](https://www.mathworks.com/help/matlab/ref/unwrap.html)
[firpmord](https://www.mathworks.com/help/signal/ref/firpmord.html)
[rining](https://en.wikipedia.org/wiki/Ringing_(signal))

Class 12
[Delay of Filter](https://www.dsprelated.com/freebooks/filters/Phase_Group_Delay.html)
[Phase Delay and Group Delay](https://dsp.stackexchange.com/questions/18435/group-delay-of-the-fir-filter)
